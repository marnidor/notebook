<?php

namespace App;
use mysqli;
use App\services\Router;

class Initialization
{
    private $result = [];

    public static function start()
    {
        self::connectDB();
    }

    private static function connectDB()
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'notebook');
    }

    public static function getAllNotes(): array
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'notebook');
        if ($connect->connect_error) {
            die("Ошибка: " . $connect->connect_error);
        }

        $query = $connect->query("SELECT * FROM notebook.user_info");
        return $query->fetch_all();
    }

    public static function getOneNote($person_id)
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'notebook');
        if ($connect->connect_error) {
            die("Ошибка: " . $connect->connect_error);
        }

        $query = $connect->query("SELECT * FROM notebook.user_info WHERE id = '$person_id'");
        $result = $query->fetch_assoc();
        Router::redirectPage('viewPersonInfo');
        return $result;
    }

    public function addNewPerson()
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'notebook');
        if ($connect->connect_error) {
            die("Ошибка: " . $connect->connect_error);
        }

        $name = $connect->real_escape_string($_POST['name']);
        $surname = $connect->real_escape_string($_POST['surname']);
        $patronymic = $connect->real_escape_string($_POST['patronymic']);
        $dateBirth = $connect->real_escape_string($_POST['date_birth']);
        $email = $connect->real_escape_string($_POST['email']);
        $phone = $connect->real_escape_string($_POST['phone']);
        $company = $connect->real_escape_string($_POST['company']);
        //$avatar = $connect->real_escape_string($_POST['date_birth']);

        $connect->query("INSERT INTO notebook.user_info (user_name, user_surname, user_patronymic, user_date_birth, user_photo, user_email, user_phone, user_company) VALUES ('$name','$surname', '$patronymic', '$dateBirth', 'photo', '$email', '$phone', '$company')");
        echo 'Add person';
        Router::redirectPage('home');
    }

    public static function updatePersonInfo($person_id)
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'notebook');
        if ($connect->connect_error) {
            die("Ошибка: " . $connect->connect_error);
        }
        echo 'update info';

        $id = $_POST['id'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $patronymic = $_POST['patronymic'];
        $dateBirth = $_POST['date_birth'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $company = $_POST['company'];

        $updatePerson = $connect->query("UPDATE notebook.user_info SET 
                              user_name = '$name',
                              user_surname = '$surname', 
                              user_patronymic = '$patronymic', 
                              user_date_birth = '$dateBirth', 
                              user_photo = 'photo', 
                              user_email = '$email', 
                              user_phone = '$phone', 
                              user_company = '$company' 
                                                      WHERE 
                                                            user_info.id = '$id'");
        Router::redirectPage('home');

        return $updatePerson->fetch_assoc();
    }

    public static function deletePersonInfo($person_id)
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'notebook');
        if ($connect->connect_error) {
            die("Ошибка: " . $connect->connect_error);
        }

        $connect->query("DELETE FROM notebook.user_info WHERE id = '$person_id'");
        Router::redirectPage('home');

    }

    public function setValues(array $array)
    {
        $this->result = $array;
    }

    public function getValuesFromDb()
    {
        return $this->result;
    }
}




