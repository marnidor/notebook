<?php

use App\services\Router;

Router::openPage('/', 'home');

$idView = strripos($_SERVER['REQUEST_URI'], '/viewPersonInfo?id=');
$idUpdate = strripos($_SERVER['REQUEST_URI'], '/updatePersonInfo?id=');
$idDelete = strripos($_SERVER['REQUEST_URI'], '/delete/upload?id=');

if (is_int($idView)) {
    Router::openPage($_SERVER['REQUEST_URI'], 'viewPersonInfo');
}

if (is_int($idUpdate)) {
    Router::openPage($_SERVER['REQUEST_URI'], 'updatePersonInfo');
}

if (is_int($idDelete)) {
    App\Initialization::deletePersonInfo($_GET['id']);
}

Router::openPage('/addNewPerson', 'addNewPerson');

Router::compareUri();

