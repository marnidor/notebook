<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<body>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php'?>
</div>
<body>
<?php
$users = \App\Initialization::getAllNotes();
foreach ($users as $user) {
    ?>
            <div class="container py-3 justify-content-center"
            <main>
                <div class="row row-cols-1 row-cols-md-3 mb-3 text-center justify-content-center">
                    <div class="col">
                    <div class="card mb-4 rounded-3 shadow-sm">
                        <div class="card-header py-3">
                            <h4 class="my-0 fw-normal"><?= $user[2] . ' '; ?><?= $user[1]; ?></h4>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled mt-3 mb-4">
                                <li>Name: <?= $user[1];?></li>
                                <li>Surame: <?= $user[2];?></li>
                                <li>Patronymic: <?= $user[3];?></li>
                                <li>Date birth: <?= $user[4];?></li>
                                <li>Email: <?= $user[6];?></li>
                                <li>Phone: <?= $user[7];?></li>
                                <li>Company: <?= $user[8];?></li>
                            </ul>
                            <button type="button" class=" btn btn-lg btn-outline-primary"><a class="text-decoration-none text-reset" href="/viewPersonInfo?id=<?= $user[0];?>">View</a></button>
                            <button type="button" class=" btn btn-lg btn-outline-warning"><a class="text-decoration-none text-reset" href="/updatePersonInfo?id=<?= $user[0];?>">Edit</a></button>
                            <button type="button" class=" btn btn-lg btn-outline-danger"><a class="text-decoration-none text-reset" href="/delete/upload?id=<?= $user[0];?>">Delete</a></button>
                        </div>
                    </div>
                </div>


            </div>

            <?php
                }
        ?>
        </main>
</body>
