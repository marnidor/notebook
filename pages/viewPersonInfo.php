<?php
$person_id = $_GET['id'];
$person = \App\Initialization::getOneNote($person_id);
?>

<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<body>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php'?>
</div>
<body>
    <div class="row row-cols-1 row-cols-md-3 mb-3 text-center justify-content-center">
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                        <h2>Information</h2>
                        <li>Name: <?= $person['user_name'];?></li>
                        <li>Surname: <?= $person['user_surname'];?></li>
                        <li>Patronymic: <?= $person['user_patronymic'];?></li>
                        <li>Date birth: <?= $person['user_date_birth'];?></li>
                        <li>Email: <?= $person['user_email'];?></li>
                        <li>Phone number: <?= $person['user_phone'];?></li>
                        <li>Company: <?= $person['user_company'];?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
